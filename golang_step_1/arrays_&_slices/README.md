# Массивы
-------
#### Размер массива является частье его типа

**Инициализация по-умолчанию:**

```go
var a1 [3]int // [0, 0, 0]
```

### Определение размера при объявлении:
##### Многоточие говорит, чтобы туда записалось длина эл-ов, которые записаны далее

```go
a3 := [...]int{1, 2, 3}
```

# Слайс -

Более сложная структура. Состоит из массива с фиксированной длиной (capacity/cap) в которую записано меньшее количество элементов (length/len). Как только length превышает capacity, размер capacity удваивается

---

#### Создание:

```go
var buf0 []int // len=0 cap=0
var buf1 []int{} // len=0 cap=0
var buf2 []int{42} // len=1 cap=1
```

**make - встроенная фун-я, которая создаёт массив определенной длины и капасити**

```go
var buf3 make([]int, 0) // len=0 cap=0
var buf4 make([]int, 5) // len=5 cap=5
var buf5 make([]int, 5, 10) // len=5 cap=10
```

#### Добавление эл-та в слайс

```go
var buf []int // len=0, cap=0
buf = append(buf, 9, 10) // len=2, cap=2

//Если длина начинает превышать capacity,
//рантайм делает х2 от предыдущего размера

buf = append(buf, 12) // len=3, cap=4

//Конкатенация

otherBuf := make([]int, 3) // [0, 0, 0]
buf = append(buf, otherBuf...) // len=6, cap=8

//Просмотр инфо-ии о слайсе,
//соответсвенно длина и капасити

var bufLen, bufCap int = len(buf), cap(buf)

```

#### Получение среза, указывающего на ту же память

```go

buf := []int{1, 2, 3, 4, 5}

sl1 := buf[1:4] // [2, 3, 4]
sl2 := buf[:2] // [1 , 2]
sl3 := buf[2:] // [3, 4, 5]

```

_Если изменить размерность слайса - увеличится его каписити, что приведет к выделению большего кол-ва памяти, т.е. это уже будет ссылка на другую область памяти_ 

```go
  newBuf := buf[:] // [1 ,2 ,3 ,4 ,5]

  newBuf = append(newBuf, 6)

// buff - не изменится  
```

#### Копирование (и только так)

```go
  newBuf = make([]int, len(buf), len(buf))
  copy(newBuf, buf)
```

# Map (хеш-таблица)
---

**Инициализация при создании**

```go
var user map[string]string = map[string]string {
  "name": "Vasya",
  "lastName": "Golybcov"
}

//С нужной ёмкостью

profile := make(map[string]string, 10)

//Кол-во эл-ов

mapLength := len(user)
```

**Проверка на существовани ключа**

```go
Вторая переменная вернёт true/false

mName, mNameExist := user["middleName"]
fmt.Println(mName, mNameExist)

// если переменная не нужна, нужна только проверка на существование ставим _

_, mNameExist := user["middleName"]

// удаление ключа
delete(user, "lastName")
```
