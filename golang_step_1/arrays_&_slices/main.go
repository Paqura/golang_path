package main

import (
	"fmt"
)

func main() {
	// Slice, он может иметь бесконечное кол-во аргументов
	var langs []string
	// Array в Go это что-то вроде конечного списка
	var arr [2]string

	arr[0] = "Go"
	arr[1] = "Js"
	
	langs = append(langs, "Go")
	langs = append(langs, "Javascript")
	langs = append(langs, "Kotlin")

	fmt.Println("Slice: ", langs)
	fmt.Println("Arr: ", arr)
}