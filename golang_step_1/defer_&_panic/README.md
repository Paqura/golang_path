# Отложенный вызов (defer)
---

Для вызова отложенной функции применяется зарезервированное слово **defer**

```go
  // Порядок выполнение deferov снизу вверх,
  // т.е сначала Afrer 2, потом After

  defer fmt.Println("After")  
  defer fmt.Println("After 2")
  fmt.Println("Before")
```

# Panic -
**служебная функция, которая крашит всю программу**

---
