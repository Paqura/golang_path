package main

import (
	"fmt"
)

func main() {
	defer fmt.Println("After")
	defer fmt.Println("After 2")
	fmt.Println("Before")
}
