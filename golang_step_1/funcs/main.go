package main

import (
	"os"
	"fmt"
	"time"
	"errors"
)

func main() {
	hourOfDay := time.Now().Hour()
	greeting, err := getGreeting(hourOfDay)
	if err != nil {
		fmt.Println(err)
    os.Exit(1)
	}
	fmt.Println(greeting)
}

func getGreeting(hour int) (string, error) {
	var message string

	if hour < 7 {
		err := errors.New("Слишком рано")
		return message, err
	}
	if hour < 12 {
		message = "Good morning"
	} else {
		message = "Bad morning :D"
	}
	return message, nil;
}