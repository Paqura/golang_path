package main

import (
	"fmt"
)

type jumper interface {
	jump() string
}

type man struct {
	name string
	age int
}

type horse struct {
	weight int
}

func main() {
	jumpers := getJumpersList()
	
	for _, jumper := range jumpers {
		fmt.Println(jumper.jump())
		//Vasya может прыгать
    //Gosha не может прыгать
    //лошадка не прыгает, жирновата
	}
}

func (m man) jump() string {
  if m.age < 60 {
		return m.name + " может прыгать"
	}
	return m.name + " не может прыгать"
}

func (h horse) jump() string {
	if h.weight > 199 {
		return "лошадка не прыгает, жирновата"
	}
	return "лошадка запрыгала"
}

func getJumpersList() []jumper {
	vasya := &man{name: "Vasya", age: 20}
	gosha := &man{name: "Gosha", age: 70}
	loshadka := &horse{weight: 200}

	list := []jumper{vasya, gosha, loshadka}
	return list
}