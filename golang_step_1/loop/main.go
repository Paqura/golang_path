package main

import (
	"fmt"
)

// вроде как в go нет ничего кроме for

func main() {
	// Случай 1

	// for i := 0; i < 5; i++ {
	// 	fmt.Println(i)
	// }
	
	// Случай 2 делает тоже самое

	i := 0

	for {
		if i >= 5 {
			break
		}
		fmt.Println(i)
		i++
	}
}