package main

import (
	"fmt"

	"./hello/model"
)

func main() {
	letters := model.GetSomeData()

	for _, letter := range letters {
		fmt.Println(letter)
	}
}
