package main

import (
	"fmt"
)

type man struct {
	name    string
	age     int
	isAdult bool
}

func main() {
	vasya := &man{name: "Vasya", age: 22}
	gosha := &man{name: "Gosha", age: 20}

	checkAdult(vasya)
	checkAdult(gosha)

	fmt.Println(vasya)
	fmt.Println(gosha)
}

func checkAdult(m *man) {
	m.isAdult = m.age >= 21
}
