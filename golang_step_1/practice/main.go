package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func uniq(input io.Reader, output io.Writer) error {
	in := bufio.NewScanner(input)
	var prevElement string

	for in.Scan() {
		currentElement := in.Text()

		if currentElement == prevElement {
			continue
		}

		if currentElement < prevElement {
			return fmt.Errorf("file not sorted")
		}

		prevElement = currentElement
		fmt.Fprintln(output, currentElement)
	}
	return nil
}

func main() {
	err := uniq(os.Stdin, os.Stdout)
	if err != nil {
		panic(err.Error())
	}
}
