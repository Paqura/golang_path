package main

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

var testOkData = `1
2
2
3
4
5
`

var correctTestResult = `1
2
3
4
5
`

func TestOk(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testOkData))
	out := new(bytes.Buffer)

	err := uniq(in, out)
	if err != nil {
		t.Errorf("test for Ok failed - error")
	}

	if out.String() != correctTestResult {
		t.Errorf("test for Ok failed - result not match")
	}
}

var testFail = `1
2
1`

func TestFailForSort(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testFail))
	out := new(bytes.Buffer)

	err := uniq(in, out)
	if err == nil {
		t.Errorf("test for Ok failed - data doesn't sort")
	}
}
