package main

import (
	"fmt"
)

func main() {
	langs := getLangs()
	
	// За место _ может стоять i - индекс,
	// но если он не используется компилятор выдаст ошибку
	// эта приблуда защищает от этого
	
	for _, element := range langs {
		fmt.Println(element)
	}
}

func getLangs() []string {
	var langsList = []string{"Js", "Go", "Kotlin"}
	return langsList
}