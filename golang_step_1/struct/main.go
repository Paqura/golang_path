package main

import (
	"fmt"
)

// как и во многих других языках

type man struct {
	name string
	age int
}

func main() {
	vasya := man{name: "Vasya", age: 100}
	gosha := man{name: "Gosha", age: 666}

	fmt.Println(vasya.jump())
	fmt.Println(gosha.jump())
}

func (m man) jump() string {
	if m.age < 101 {
		return m.name + " молодой"
	}
	return m.name + " старикос"
}