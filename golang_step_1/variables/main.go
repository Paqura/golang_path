package main

import (
	"fmt"
)

func main() {
	// авто datatypes
	f := 2
	fmt.Println(f)
	// объявление
	var f1 string = "2"
	var f2 int = 2
	var f3 bool = true
	var f4 = []string{"Hello", "world"}
	f4[3] = "2"
	fmt.Println(f1, f2, f3, f4)
}
